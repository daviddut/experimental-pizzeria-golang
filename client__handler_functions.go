package main

import (
	"fmt"
	"net/http"
	"time"
)

// Error ...
func Error(w http.ResponseWriter, r *http.Request) {
	tmpl.ExecuteTemplate(w, "Error", 0)
}

// Success ...
func Success(w http.ResponseWriter, r *http.Request) {
	tmpl.ExecuteTemplate(w, "Success", 0)
}

// CreateOrder ...
func CreateOrder(w http.ResponseWriter, r *http.Request) {
	db := initDbConn()
	selDB, err := db.Query(SelectAllIngredients)
	if err != nil {
		panic(err.Error())
	}
	ingredient := Ingredients{}
	res := []Ingredients{}
	for selDB.Next() {
		var id, stock int
		var name string
		var price float32
		err = selDB.Scan(&id, &name, &price, &stock)
		if err != nil {
			panic(err.Error())
		}
		ingredient.ID = id
		ingredient.Name = name
		ingredient.Price = price
		ingredient.Stock = stock
		res = append(res, ingredient)
	}
	tmpl.ExecuteTemplate(w, "CreateOrder", res)
	defer db.Close()
}

// code taken from: https://play.golang.org/p/tWAVrtVMVK
// parseTimeStrict parses a formatted string and returns the time value it
// represents. The output is identical to time.Parse except it returns an
// error for strings that don't format to the input value.
//
// An example where the output differs from time.Parse would be:
// parseTimeStrict("1/2/06", "11/31/15")
//	- time.Parse returns "2015-12-01 00:00:00 +0000 UTC"
//	- parseTimeStrict returns an error
func parseTimeStrict(layout, value string) (time.Time, error) {
	t, err := time.Parse(layout, value)
	if err != nil {
		return t, err
	}
	if t.Format(layout) != value {
		return t, fmt.Errorf("invalid time: %q", value)
	}
	return t, nil
}

//https://golang.org/src/time/format.go
func testDate(s string) bool {
	t, err := parseTimeStrict("2006-01-02", s)
	if err != nil {
		fmt.Printf("error parsing %q: %v\n", s, err)
		return false
	}
	fmt.Printf("parsed %q: %v\n", s, t)
	return true
}

// InsertOrder insert an order into the database.
func InsertOrder(w http.ResponseWriter, r *http.Request) {

	db := initDbConn()
	if r.Method == "POST" {
		r.ParseForm()
		customerID := r.FormValue("id")
		ingredientsSelected := r.Form["ingredients_list"]
		qty := r.FormValue("qty")
		deliveredDate := r.FormValue("dateLivraison")
		placedDate := time.Now().Local()

		var isCustomerValid bool
		// Validate that the customer can be found in the database.
		errCustomer := db.QueryRow(SelectCustomerID, customerID).Scan(&isCustomerValid)
		if errCustomer != nil {
			println("Error:", errCustomer.Error())
		}

		// Sanity check
		if customerID == "" || qty == "" || deliveredDate == "" || !isCustomerValid ||
			len(ingredientsSelected) == 0 || !testDate(deliveredDate) {
			http.Redirect(w, r, "/error", 301)
		} else {
			//Proceed with order creation

			// Insert a new pizza
			dbPizzaStat, err := db.Prepare("INSERT INTO pizza(qty) VALUES(?)")
			if err != nil {
				println("Error:", err.Error())
			}
			res, err := dbPizzaStat.Exec(qty)
			if err != nil {
				println("Error:", err.Error())
			}

			// Get the last inserted pizza id from the current DB connection
			pizzaID, err := res.LastInsertId()
			if err == nil {
				println("LastInsert pizzaID:", pizzaID)
			} else {
				println("Error:", err.Error())
			}

			// Insert a new order
			dbOrderStat, err := db.Prepare("INSERT INTO orders(customer_id, placed_date, delivery_date) VALUES(?,?,?)")
			if err != nil {
				println("Error:", err.Error())
			}
			resOrder, err := dbOrderStat.Exec(customerID, placedDate, deliveredDate)
			if err != nil {
				println("Error:", err.Error())
			}

			// Get the last inserted order id from the current DB connection
			OrderID, err := resOrder.LastInsertId()
			if err == nil {
				println("LastInsert OrderID:", OrderID)
			} else {
				println("Error:", err.Error())
			}

			// for all ingredients selected, insert an order_item
			for _, ingredientID := range ingredientsSelected {
				fmt.Printf("%+v\n", ingredientID)
				dbOrderItemStat, err := db.Prepare("INSERT INTO order_item(order_id, ingredient_id, pizza_id) VALUES(?,?,?)")
				if err != nil {
					println("Error:", err.Error())
				}
				resOrderItem, err2 := dbOrderItemStat.Exec(OrderID, ingredientID, pizzaID)
				if err2 != nil {
					println("Error:", err.Error())
				}
				fmt.Printf("%+v\n", resOrderItem)
			}

		}
		defer db.Close()
		http.Redirect(w, r, "/success", 301)
	}
}
