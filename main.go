package main

import (
	"html/template"
	"log"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
)

var tmpl = template.Must(template.ParseGlob("form/*"))

func main() {
	log.Println("Server started on: http://localhost:3000")
	http.HandleFunc("/", CreateOrder)
	http.HandleFunc("/ingredients", ListIngredients)
	http.HandleFunc("/orders", ListOrders)
	http.HandleFunc("/insert", InsertOrder)
	http.HandleFunc("/error", Error)
	http.HandleFunc("/success", Success)
	http.ListenAndServe("0.0.0.0:3000", nil)
}
