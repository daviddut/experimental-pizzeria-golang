package main

import (
	"net/http"
)

// ListIngredients List all ingredients, their prices and stock
func ListIngredients(w http.ResponseWriter, r *http.Request) {
	db := initDbConn()
	selDB, err := db.Query(SelectAllIngredients)
	if err != nil {
		panic(err.Error())
	}
	ingredient := Ingredients{}
	res := []Ingredients{}
	for selDB.Next() {
		var id, stock int
		var name string
		var price float32
		err = selDB.Scan(&id, &name, &price, &stock)
		if err != nil {
			panic(err.Error())
		}
		ingredient.ID = id
		ingredient.Name = name
		ingredient.Price = price
		ingredient.Stock = stock
		res = append(res, ingredient)
	}
	tmpl.ExecuteTemplate(w, "Ingredients", res)
	defer db.Close()
}

// ListOrders return a list of all information related to existing orders
func ListOrders(w http.ResponseWriter, r *http.Request) {
	db := initDbConn()
	selDB, err := db.Query(SelectOrdersAndCustomers)
	if err != nil {
		panic(err.Error())
	}
	order := OrdersInformation{}
	res := []OrdersInformation{}
	for selDB.Next() {
		var id, customerID int
		var placedDate, deliveryDate, firstname, lastname, city, houseNumber, street, phone string

		err = selDB.Scan(&id,
			&customerID,
			&placedDate,
			&deliveryDate,
			&firstname,
			&lastname,
			&city,
			&houseNumber,
			&street,
			&phone)

		if err != nil {
			panic(err.Error())
		}

		order.ID = id
		order.CustomerID = customerID
		order.PlacedDate = placedDate
		order.DeliveryDate = deliveryDate
		order.FirstName = firstname
		order.LastName = lastname
		order.City = city
		order.HouseNumber = houseNumber
		order.Street = street
		order.Phone = phone

		// Retrieve pizzas in the current order
		selPizzaDB, err := db.Query(SelectPizzasInOrder, order.ID)
		if err != nil {
			panic(err.Error())
		}

		var pizzasPrice float32
		pizza := Pizza{}
		pizzaList := []Pizza{}
		for selPizzaDB.Next() {
			var pizzaID, pizzaQty int
			var pizzaPrice float32
			err = selPizzaDB.Scan(&pizzaID, &pizzaQty)
			if err != nil {
				panic(err.Error())
			}
			pizza.ID = pizzaID
			pizza.Qty = pizzaQty

			// Retrieve a list of ingredients for the current pizza
			selPizzaIngredientsDB, err := db.Query(SelectPizzaIngredients, pizza.ID)
			if err != nil {
				panic(err.Error())
			}

			ingredient := Ingredients{}
			ingredientList := []Ingredients{}
			for selPizzaIngredientsDB.Next() {
				var name string
				var price float32
				err = selPizzaIngredientsDB.Scan(&name, &price)
				if err != nil {
					panic(err.Error())
				}
				ingredient.Name = name
				ingredient.Price = price
				pizzaPrice += price
				ingredientList = append(ingredientList, ingredient)

			}
			pizzasPrice += pizzaPrice * float32(pizzaQty)
			order.Bill = pizzasPrice
			pizza.Ingredients = ingredientList
			pizzaList = append(pizzaList, pizza)
		}

		order.Pizza = pizzaList
		res = append(res, order)
	}

	tmpl.ExecuteTemplate(w, "Orders", res)
	defer db.Close()
}
