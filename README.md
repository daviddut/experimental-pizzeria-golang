# Purpose

The purpose of this document is to describe the experimental Pizzeria ordering system.

It was implemented in Go and uses server side rendering with a MySQL database.

The application requires a version of Go >= 1.8.
