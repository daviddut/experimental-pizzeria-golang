package main

// SelectAllIngredients query
const SelectAllIngredients = "SELECT * FROM ingredients ORDER BY stock ASC"

// SelectPizzasInOrder query
const SelectPizzasInOrder = "select id, qty from `pizza` where id in(select DISTINCT `pizza_id` from `order_item` where `order_item`.order_id=?)"

// SelectPizzaIngredients query
const SelectPizzaIngredients = "select name, price from `order_item` INNER JOIN `ingredients`" +
	"ON `order_item`.ingredient_id = `ingredients`.id WHERE `order_item`.pizza_id=?"

// SelectOrdersAndCustomers query
const SelectOrdersAndCustomers = "select `orders`.id, `orders`.customer_id, `orders`.placed_date, `orders`.delivery_date, " +
	"`customers`.firstname, `customers`.lastname, `customers`.city, `customers`.house_number, `customers`.street, `customers`.phone " +
	"from orders INNER JOIN `customers` ON `orders`.customer_id = `customers`.id"

// SelectCustomerID query
const SelectCustomerID = "select COUNT(id) from `customers` where id=?"
