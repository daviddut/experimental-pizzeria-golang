package main

// Ingredients struct to map to DB table
type Ingredients struct {
	ID    int
	Name  string
	Price float32
	Stock int
}

// Pizza struct
type Pizza struct {
	ID          int
	Qty         int
	Ingredients []Ingredients
}

// OrdersInformation struct thats aggregate information from the following DB tables:
// - orders
// - customers
// - order_item
// - ingredients
type OrdersInformation struct {
	ID           int
	CustomerID   int
	PlacedDate   string
	DeliveryDate string
	FirstName    string
	LastName     string
	City         string
	HouseNumber  string
	Street       string
	Phone        string
	Pizza        []Pizza
	Bill         float32
}

/**


  // select `orders`.id, `orders`.customer_id, `orders`.placed_date, `orders`.delivery_date,
  	// `customers`.firstname, `customers`.lastname, `customers`.city, `customers`.house_number, `customers`.street, `customers`.phone
  	// from orders INNER JOIN `customers` ON `orders`.customer_id = `customers`.id

*/
