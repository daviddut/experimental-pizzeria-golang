DROP DATABASE IF EXISTS `pizzeria`;
CREATE DATABASE pizzeria;
use pizzeria;

-- DROP TABLE IF EXISTS `order_item`;
-- DROP TABLE IF EXISTS `orders`;
-- DROP TABLE IF EXISTS `ingredients`;
-- DROP TABLE IF EXISTS `customers`;
-- DROP TABLE IF EXISTS `pizza`;

set names utf8;

CREATE TABLE `ingredients` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `price` decimal(4,2) NOT NULL,
  `stock` int(6) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `customers` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `city` varchar(30) NOT NULL,
  `house_number` varchar(30) NOT NULL,
  `street` varchar(30) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


CREATE TABLE `orders` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(6) unsigned NOT NULL,
  `placed_date` DATE NOT NULL,
  `delivery_date` DATE NOT NULL,
  FOREIGN KEY fk_customer(customer_id) 
  REFERENCES `customers`(id)
  ON DELETE CASCADE,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


CREATE TABLE `pizza` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `qty` int(6) unsigned NOT NULL,
   PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


CREATE TABLE `order_item` (
  `order_id` int(6) unsigned NOT NULL,
  `ingredient_id` int(6) unsigned NOT NULL,
  `pizza_id` int(6) unsigned NOT NULL,
  FOREIGN KEY fk_order(order_id) 
  REFERENCES `orders`(id)
  ON DELETE CASCADE,
  FOREIGN KEY fk_ingredients(ingredient_id) 
  REFERENCES `ingredients`(id)
  ON DELETE CASCADE,
  FOREIGN KEY fk_pizza(pizza_id) 
  REFERENCES `pizza`(id)
  ON DELETE CASCADE,
  PRIMARY KEY (`order_id`,`ingredient_id`, `pizza_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


INSERT INTO `customers` ( `firstname`, `lastname`, `city`,`house_number`,`street`,`phone`,`email`,`password`)
VALUES ( "John", "Doe", "Saint-Bruno", "7", "Boul. Gerard", "555-666-7777", 'john@gmail.com','test');

INSERT INTO `customers` ( `firstname`, `lastname`, `city`,`house_number`,`street`,`phone`,`email`,`password`)
VALUES ( "Lucie", "Beaulieu", "Saint-Julie", "11", "deschamps", "444-632-4557", 'lucie@gmail.com','test');

/** ingredients table inserts  */
INSERT INTO `ingredients` ( `name`, `price`, `stock`)
VALUES ( "tomates", '1.0', 10 );

INSERT INTO `ingredients` ( `name`, `price`, `stock`)
VALUES ( "oignons", '0.5', 15 );

INSERT INTO `ingredients` ( `name`, `price`, `stock`)
VALUES ( "champignons", '2.0', 45 );

INSERT INTO `ingredients` ( `name`, `price`, `stock`)
VALUES ( "pepperoni", '1.5', 30 );

INSERT INTO `ingredients` ( `name`, `price`, `stock`)
VALUES ( "ananas", '3.0',  100);

INSERT INTO `ingredients` ( `name`, `price`, `stock`)
VALUES ( "poivrons", '0.75', 5 );

INSERT INTO `ingredients` ( `name`, `price`, `stock`)
VALUES ( "fromage", '1.25', 1 );

INSERT INTO `ingredients` ( `name`, `price`, `stock`)
VALUES ( "artichauts", '5.0', 20 );

INSERT INTO `ingredients` ( `name`, `price`, `stock`)
VALUES ( "olives", '2.25', 40 );

INSERT INTO `ingredients` ( `name`, `price`, `stock`)
VALUES ( "bacon", '4.0', 6 );

INSERT INTO `ingredients` ( `name`, `price`, `stock`)
VALUES ( "jambon", '3.0', 4 );

INSERT INTO `ingredients` ( `name`, `price`, `stock`)
VALUES ( "anchois", '1.25', 200 );

INSERT INTO `ingredients` ( `name`, `price`, `stock`)
VALUES ( "fromage de chèvre", '6.0', 40 );

INSERT INTO `ingredients` ( `name`, `price`, `stock`)
VALUES ( "roquette", '2.45', 55 );



/**  Order for customer # 1 */
INSERT INTO `orders` ( `customer_id`,`placed_date`, `delivery_date`)
VALUES (1, '2017-12-25', '2017-12-25');

/** insert pizza #1 with a qty of 2 */
INSERT INTO `pizza` ( `qty`) VALUES ( 2 );
/** insert pizza #2    */
INSERT INTO `pizza` ( `qty`) VALUES ( 1 );

/** Ingredients for pizza #1 */
INSERT INTO `order_item` ( `order_id`, `ingredient_id`, `pizza_id`)
VALUES ( 1, 2,1);
INSERT INTO `order_item` ( `order_id`, `ingredient_id`, `pizza_id`)
VALUES ( 1, 3,1);
INSERT INTO `order_item` ( `order_id`, `ingredient_id`, `pizza_id`)
VALUES ( 1, 8,1);

/** Ingredients for pizza #2 */
INSERT INTO `order_item` ( `order_id`, `ingredient_id`, `pizza_id`)
VALUES ( 1, 1,2);
INSERT INTO `order_item` ( `order_id`, `ingredient_id`, `pizza_id`)
VALUES ( 1, 2,2);
INSERT INTO `order_item` ( `order_id`, `ingredient_id`, `pizza_id`)
VALUES ( 1, 3,2);


/** Order #2 for customer #2 */
INSERT INTO `orders` ( `customer_id`,`placed_date`, `delivery_date`)
VALUES (2, '2018-01-25', '2018-01-26');

/** insert pizza #3 */
INSERT INTO `pizza` ( `qty`) VALUES ( 1 );

/** Ingredients for pizza #3 */
INSERT INTO `order_item` ( `order_id`, `ingredient_id`, `pizza_id`)
VALUES ( 2, 9,3);
INSERT INTO `order_item` ( `order_id`, `ingredient_id`, `pizza_id`)
VALUES ( 2, 10,3);
INSERT INTO `order_item` ( `order_id`, `ingredient_id`, `pizza_id`)
VALUES ( 2, 11,3);


/**  Order for customer # 1 */
INSERT INTO `orders` ( `customer_id`,`placed_date`, `delivery_date`)
VALUES (1, '2018-02-28', '2018-03-01');

/** insert pizza #4    */
INSERT INTO `pizza` ( `qty`) VALUES ( 1 );

INSERT INTO `order_item` ( `order_id`, `ingredient_id`, `pizza_id`)
VALUES ( 3, 13,4);
INSERT INTO `order_item` ( `order_id`, `ingredient_id`, `pizza_id`)
VALUES ( 3, 14,4);